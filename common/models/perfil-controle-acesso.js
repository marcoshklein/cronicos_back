'use strict';

module.exports = function(Perfilcontroleacesso) {
    Perfilcontroleacesso.on('dataSourceAttached', function(obj) {
        Perfilcontroleacesso.custom.autocomplete.role = {
            where: function(reg) {
                return {
                   name: (reg),
                   // pai: {neq: null}
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.name
                };
                return newRow;
            },
            label: function(row) {
                return row.role.name;
            }
        };
    })

};
