'use strict';

var _ = require('underscore');

module.exports = function(Indicador) {
    // Indicador.observe('before delete', function IndicadorValidation(ctx, next) {
    //     debugger;
    // });

    Indicador.validatesUniquenessOf('codigo', {message: 'Indicador já existente!'});

    Indicador.observe('before save', function IndicadorValidation(ctx, next) {
        var formula = null;
        var data;
        if (ctx.instance) {
            // ctx.instance["emp_id"] = empresa;
            data = ctx.instance.__data;
            data.tipo = data['tipo.label'];
        } else {
            // ctx.data["emp_id"] = empresa;
            data = ctx.data;
        }

        console.log('data.tipo', data.tipo);
        if (data.tipo.id == 3) {
            formula = JSON.parse(data.formula).formula.trim();
            console.log('formula', formula);

            var tokens = formula.split(/[0-9\.\(|\)|*|^|\-|\+|\/]/g);
            console.log('tokens', tokens);
            var tokens_unique = tokens.filter(function onlyUnique(value, index, self) {
                if (value.trim().length == 0) {
                    return false;
                }
                return self.indexOf(value) === index;
            })
            console.log('tokens unique:', tokens_unique);

            Indicador.find({
                where: {
                    codigo: { inq: tokens_unique }
                }
            }, function(err, rows) {
                if (!err) {
                    console.log('indicadores encontrados:', rows);
                    var indicadores = []; //[codigo] = tipo

                    _.each(rows, function(row, k) {
                        indicadores[row.codigo] = row.indt_id;
                    })

                    var errs = [];
                    _.each(tokens_unique, function(token, k) {
                        if (Object.keys(indicadores).indexOf(token) == -1) {
                            errs.push('O indicador {' + token + '} não foi encontrado na base de indicadores');
                        } else if (Object.keys(indicadores).indexOf(token) != -1 && [1, 3].indexOf(indicadores[token]) == -1) {
                            errs.push('O indicador {' + token + '} não é do tipo numérico ou resultado');
                        }
                    })

                    if (errs.length > 0) {
                        var createErr = new Error(errs.join(','));
                        createErr.statusCode = 422;
                        createErr.code = 'CANT_SAVE_MODEL';

                        next(createErr);
                    } else {
                        next();
                    }
                }
            });
        } else {
            next();
        }
    });

    Indicador.on('dataSourceAttached', function(obj) {
        Indicador.custom.autocomplete.tipo = {
            where: function(reg) {
                return {
                    nome: (reg)
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function(row) {


                return row.tipo.nome;
            }
        };

        Indicador.custom.autocomplete.unidade_medida = {
            where: function(reg) {
                return {
                    nome: (reg)
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function(row) {


                return row.unidade_medida.nome;
            }
        };

        Indicador.custom.autocomplete.cid = {
            where: function(reg) {
                return {
                    descricao: (reg),
                    situacao: true
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.descricao
                };

                return newRow;
            },
            label: function(row) {
                return row.cid.descricao;
            }
        };
    })
};
