'use strict';

module.exports = function(Medicamento) {
  Medicamento.observe('before save', function completeNomeApresentacao(ctx, next) {
    if (ctx.instance && ctx.instance['produto.label']) {
      // User created medicamento
      ctx.instance.nome_apresentacao = ctx.instance['produto.label'].label + ' - ' + ctx.instance['principio_ativo_medicamentos.label'].label + ' - ' + ctx.instance.apresentacao;
    } else if (ctx.data) {
      // User update medicamento
      ctx.data.nome_apresentacao = ctx.data['produto.label'] + ' - ' + ctx.data['principio_ativo_medicamentos.label'] + ' - ' + ctx.data.apresentacao;
    }
    // else .. populate
    next();
  });
  Medicamento.on('dataSourceAttached', function(obj) {
    // console.log('tango telefone');
    Medicamento.custom.autocomplete.tipo = {
      where: function(reg) {
        return {
          nome: (reg),
        };
      },
      out: function(row) {
        var newRow = {
          id: row.id,
          label: row.nome,
        };
        return newRow;
      },
      label: function(row) {
        return row.tipo.nome;
      },
    };
    Medicamento.custom.autocomplete.produto = {
      where: function(reg) {
        return {
          nome: (reg),
        };
      },
      out: function(row) {
        var newRow = {
          id: row.id,
          label: row.nome,
        };
        return newRow;
      },
      label: function(row) {
        return row.produto.nome;
      },
    };
    Medicamento.custom.autocomplete.principio_ativo_medicamentos = {
      where: function(reg) {
        return {
          nome: (reg),
        };
      },
      out: function(row) {
        var newRow = {
          id: row.id,
          label: row.nome,
        };
        return newRow;
      },
      label: function(row) {
        return row.principio_ativo_medicamentos.nome;
      },
    };
    Medicamento.custom.autocomplete.classe_terapeutica = {
      where: function(reg) {
        return {
          nome: (reg),
        };
      },
      out: function(row) {
        var newRow = {
          id: row.id,
          label: row.nome,
        };
        return newRow;
      },
      label: function(row) {
        return row.classe_terapeutica.nome;
      },
    };
    Medicamento.custom.autocomplete.laboratorio = {
      where: function(reg) {
        return {
          nome: (reg),
        };
      },
      out: function(row) {
        var newRow = {
          id: row.id,
          label: row.nome,
        };
        return newRow;
      },
      label: function(row) {
        return row.laboratorio.nome;
      },
    };
    Medicamento.custom.autocomplete.tarja = {
      where: function(reg) {
        return {
          nome: (reg),
        };
      },
      out: function(row) {
        var newRow = {
          id: row.id,
          label: row.nome,
        };
        return newRow;
      },
      label: function(row) {
        return row.tarja.nome;
      },
    };
  });
};
