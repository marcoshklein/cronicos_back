'use strict';

var LoopBackContext = require('loopback-context');

module.exports = function(Atendimento) {

    Atendimento.observe('before save', function(ctx, next) {
        // New atendimento
        if (ctx.instance) {
            // console.log('New atendimento...')
            // console.log('Ctx instance: ', ctx.instance)
            if (ctx.instance.inicio === undefined) {
                ctx.instance.inicio = new Date();
            }
        // Edit atendimento
        } else {
            // console.log('Edit atendimento...')
            // console.log('Ctx data: ', ctx.data)
            if (ctx.data.inicio === undefined) {
                ctx.data.inicio = new Date();
            }
        }
        return next();
    });

    Atendimento.on('dataSourceAttached', function(obj) {
        Atendimento.custom.autocomplete.prestador = {
            where: function(reg) {
                return {
                    nome: (reg),
                    situacao: true
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function(row) {
                return row.prestador.nome;
            }
        };

        Atendimento.custom.autocomplete.paciente = {
            where: function(reg) {
                return {
                    nome: (reg),
                    situacao: true
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function(row) {
                return row.paciente.nome;
            }
        };

        Atendimento.custom.autocomplete.tipo = {
            where: function(reg) {
                return {
                    nome: (reg)
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function(row) {
                return row.tipo.nome;
            }
        };

        Atendimento.custom.autocomplete.status = {
          where: function(reg) {
            return {
              nome: (reg)
            };
          },
          out: function(row) {
            var newRow = {
              id: row.id,
              label: row.nome
            };
            return newRow;
          },
          label: function(row) {
            return row.status.nome;
          }
        };

        Atendimento.custom.autocomplete.especialidade = {
        where: function(reg) {
          return {
            descricao: (reg),
            situacao: true
          };
        },
        out: function(row) {
          var newRow = {
            id: row.id,
            label: row.descricao
          };
          return newRow;
        },
        label: function(row) {
          return row.especialidade.descricao;
        }
      };
    });

    Atendimento.createWithType = createWithType;
    Atendimento.remoteMethod('createWithType', {
      description: 'Creates new atendimento with type',
      accepts: ([
        {
          arg: 'atendimento',
          type: 'object',
          description: 'Model object',
          required: true,
          http: { source: 'body' }
        },
        {
          arg: 'nome',
          type: 'any',
          description: 'Model type name',
          required: true,
          http: { source: 'path' }
        }
      ]),
      returns: { arg: 'data', type: 'object' },
      http: { verb: 'post', path: '/status/type/:nome' }
    });

    function createWithType(atendimento, nome, fn) {
      console.log('Create new atendimento type!');

      var _ctx = LoopBackContext.getCurrentContext();
      var currentUser = _ctx && _ctx.get('currentUser');

      atendimento.pres_id = currentUser.pres_id;

      var TipoAtendimento = Atendimento.app.models.TipoAtendimento;
      var StatusAtendimento = Atendimento.app.models.StatusAtendimento;

      StatusAtendimento.findOne({
        where: {
          nome : 'Em Atendimento'
        }
      }, function (err, status) {
          if (err) {
            fn(err);
          }

          atendimento.stat_id = status.id;
          TipoAtendimento.findOne({
              where: {
                nome : nome
              }
            }, function (err, tipo) {
              if (err) {
                fn(err);
              }

              atendimento.tat_id = tipo.id;
              Atendimento.create(atendimento, function (err, row) {
                if (err) {
                  fn(err);
                }

                fn(null, row);
          });
        });
      });
    }

};
