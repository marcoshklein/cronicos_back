'use strict';

var g = require('strong-globalize')();
var LoopBackContext = require('loopback-context');
var _ = require('underscore');

module.exports = function(Menu) {
    Menu.on('dataSourceAttached', function(obj) {
        Menu.custom.autocomplete.menuId = {
            where: function(reg) {
                return {
                    nome: (reg),
                    pai: null
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function(row) {
                return row.menuId.nome;
            }
        };
    })


    Menu.getMenus = getMenus;
    Menu.remoteMethod('getMenus', {
        description: 'Retrieve Menus for a logged in user.',
        accepts: [{
            arg: 'req',
            type: 'object',
            required: true,
            http: {
                source: 'body'
            }
        },
            // { arg: 'accessToken', type: 'object', http: function (ctx) { return ctx.req.accessToken; } }
        ],
        returns: {
            arg: 'menus',
            type: 'array',
            // root: true,
            description: g.f('Return array with menus'),
        },
        http: {
            verb: 'get',
            path: '/get-menus'
        }
    });

    function getMenus(req, fn) {
        console.log('GET MENUS!');
        // if (!req.accessToken) {
        //     var err = new Error(g.f('Token required'));
        //     err.statusCode = 401;
        //     err.name = 'RETRIEVE_CASOS_FAILED';
        //     fn(err);
        // } else {

        var _ctx = LoopBackContext.getCurrentContext();
        var currentUser = _ctx && _ctx.get('currentUser');
        // console.log('currentUser: ', currentUser); // voila!

        var perfil = currentUser.pusu_id;

        // console.log(currentUser);
        var includes = ['perfis'];
        // includes.push({
        //     relation: 'perfis',
        //     scope: {
        //         where: {
        //             pusu_id: currentUser.pusu_id
        //         }
        //     }

        // });


        Menu.find({
            // include: includes,
            where: {
                pai: null
            }
        }, function (err, rows) {
            if (err) {
                fn(err);
            }
            else {

                // console.log(rows);
                var _rows = [];

                var pais = {};
                // var rowIds = [];

                rows.forEach(function (row) {
                    // rowIds.push(row.id);
                    pais[row.id] = row;
                    pais[row.id].menus = [];
                });

                Menu.find({
                    include: includes,
                    where: {
                        pai: {inq: Object.keys(pais)}
                    }
                }, function (err, items) {


                    // var items = row.items();
                    // console.log(items);
                    items.forEach(function(_item) {
                        var allow;
                        // console.log('currentUser: ', currentUser)
                        if (currentUser.admin != true) {
                            if (currentUser.pres_id) {
                                if (_item.nome === 'Prontuário' || _item.nome === 'Agenda') allow = true;
                                else allow = false;
                            }

                            // @todo ACL with tipo de perfil
                            // var perfis = _item.perfis();
                            // console.log('perfis: ', perfis)
                            // perfis.some(function (perfil) {
                            //     // allow = true; return;
                            //     if (perfil.per_id == currentUser.pusu_id) {
                            //         allow = true;
                            //         return true;
                            //     }
                            //     else {
                            //         allow = false;
                            //     }
                            // });
                        }
                        else {
                            allow = true;
                        }


                        if (allow) {
                            // debugger;
                            // if (pais[_item.pai]) {

                            // }
                            // console.log(pais);
                            pais[_item.pai].menus.push(_item);
                        }
                    })

                    _.each(pais,function(pai) {
                        if (pai.menus.length != 0) {
                            _rows.push(pai);
                        }
                    });

                    fn(null, _rows);
                });


            }
        })

    };
};
