'use strict';

module.exports = function(Doenca) {

    Doenca.validatesUniquenessOf('categoria', {message: 'Doença já existente!'});

    Doenca.on('dataSourceAttached', function(obj) {
        Doenca.custom.autocomplete.categoria = {
            where: function(reg) {
                return {
                    nome: (reg)
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function(row) {

                return row.categoria.nome;
            }
        };
    });

};
