'use strict';

module.exports = function(Pacientecid) {
    Pacientecid.on('dataSourceAttached', function(obj) {
        // console.log('tango telefone');
        Pacientecid.custom.autocomplete.cid = {
            where: function(reg) {
                return {
                    descricao: (reg)
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.descricao
                };
                return newRow;
            },
            label: function(row) {
                return row.cid.descricao;
            }
        };
    });
};
