'use strict';

module.exports = function(Indicadorcid) {
    Indicadorcid.on('dataSourceAttached', function(obj) {
        Indicadorcid.custom.autocomplete.cid = {
            where: function(reg) {
                return {
                    descricao: (reg),
                    situacao: true
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.descricao
                };
                return newRow;
            },
            label: function(row) {
                return row.cid.descricao;
            }
        };
    });
};
