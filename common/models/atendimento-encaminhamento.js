'use strict';

module.exports = function (Atendimentoencaminhamento) {
  Atendimentoencaminhamento.on('dataSourceAttached', function (obj) {
    Atendimentoencaminhamento.custom.autocomplete.prestador = {
      where: function(reg) {
        return {
          nome: (reg),
          situacao: true
        };
      },
      out: function(row) {
        var newRow = {
          id: row.id,
          label: row.nome
        };
        return newRow;
      },
      label: function(row) {
        return row.prestador.nome;
      }
    };

    Atendimentoencaminhamento.custom.autocomplete.especialidade = {
      where: function(reg) {
        return {
          descricao: (reg),
          situacao: true
        };
      },
      out: function(row) {
        var newRow = {
          id: row.id,
          label: row.descricao
        };
        return newRow;
      },
      label: function(row) {
        return row.especialidade.descricao;
      }
    };

  });
};
