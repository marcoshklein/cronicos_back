'use strict';

module.exports = function(Documentomedico) {
    Documentomedico.on('dataSourceAttached', function(obj) {
      Documentomedico.custom.autocomplete.documento_tipo = {
        where: function(reg) {
          return {
            nome: (reg)
          };
        },
        out: function(row) {
          var newRow = {
            id: row.id,
            label: row.nome
          };
          return newRow;
        },
        label: function(row) {
          return row.documento_tipo.nome;
        }
      };
      Documentomedico.custom.autocompleteGeneral = {
        where: function (reg) {
          return {
            nome: (reg)
          };
        },
        out: function (row) {
          row.label = row.nome;
          return row;
        },
        label: function (row) {
          return row.nome;
        }
      };
    });
};
