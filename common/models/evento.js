'use strict';

var LoopBackContext = require('loopback-context');

module.exports = function(Evento) {

    Evento.on('dataSourceAttached', function(obj) {
      Evento.custom.autocomplete.prestador = {
        where: function(reg) {
          return {
            nome: (reg),
            situacao: true
          };
        },
        out: function(row) {
          var newRow = {
            id: row.id,
            label: row.nome
          };
          return newRow;
        },
        label: function(row) {
          return row.prestador.nome;
        }
      };

      Evento.custom.autocomplete.paciente = {
        where: function(reg) {
          return {
            nome: (reg),
            situacao: true
          };
        },
        out: function(row) {
          var newRow = {
            id: row.id,
            label: row.nome
          };
          return newRow;
        },
        label: function(row) {
          return row.paciente.nome;
        }
      };

      Evento.custom.autocomplete.tipo = {
        where: function(reg) {
          return {
            nome: (reg)
          };
        },
        out: function(row) {
          var newRow = {
            id: row.id,
            label: row.nome
          };
          return newRow;
        },
        label: function(row) {
          return row.tipo.nome;
        }
      };

      Evento.custom.autocomplete.status = {
        where: function(reg) {
          return {
            nome: (reg)
          };
        },
        out: function(row) {
          var newRow = {
            id: row.id,
            label: row.nome
          };
          return newRow;
        },
        label: function(row) {
          return row.status.nome;
        }
      };

    });

    Evento.getEventosFromEmpresa = getEventosFromEmpresa;
    Evento.remoteMethod('getEventosFromEmpresa', {
      description: 'Retrieves empresa\'s eventos from empresa',
      accessType: 'READ',
      accepts: ([]),
      returns: { arg: 'data', type: 'array', root: true },
      http: { verb: 'get', path: '/empresa' }
    });

    function getEventosFromEmpresa(fn) {
      console.log('Get eventos!');

      var _ctx = LoopBackContext.getCurrentContext();
      var currentUser = _ctx && _ctx.get('currentUser');

      Evento.find({
        include: [
          'paciente',
          'prestador',
          'atendimento',
          'tipo',
          'status'
        ],
        where: { emp_id: currentUser.emp_id },
        order: ['start DESC']
      }, function (err, rows) {
        if (err) {
          fn(err);
        }
        else {
          var newRows = new Array();
          rows.forEach(function (row) {
            var newRow = (JSON.parse(JSON.stringify(row)));
            Object.keys(newRow).forEach(function (attr) {
              if (newRow[attr]) {
                if (typeof newRow[attr] === 'object') {
                  newRow[attr + '.label'] = newRow[attr].nome;
                }
              }
            });
            newRows.push(newRow);
          });
          // console.log(rows)
          fn(null, newRows);
        }
      });
    }
};
