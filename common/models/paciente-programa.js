'use strict';

module.exports = function(PacientePrograma) {
    PacientePrograma.on('dataSourceAttached', function(obj) {
        PacientePrograma.custom.autocomplete.convenio = {
            where: function(reg) {
                return {
                    codigo: (reg),
                    situacao: true
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.codigo
                };
                return newRow;
            },
            label: function(row) {
                return row.convenio.codigo;
            }
        };

        PacientePrograma.custom.autocomplete.programa = {
            where: function(reg) {
                return {
                    descricao: (reg),
                    situacao: true
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.descricao
                };
                return newRow;
            },
            label: function(row) {
                return row.programa.descricao;
            }
        };
    })

};