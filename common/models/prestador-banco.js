'use strict';

module.exports = function(PrestadorBanco) {
    PrestadorBanco.on('dataSourceAttached', function(obj) {
        PrestadorBanco.custom.autocomplete.banco = {
            where: function(reg) {
                return {
                    banco_name: (reg)
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.banco_code + ' - ' + row.banco_name
                };
                return newRow;
            },
            label: function(row) {
                return row.banco.banco_code + ' - ' + row.banco.banco_name;
            }
        };
    })

};
