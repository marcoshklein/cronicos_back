'use strict';

module.exports = function(Prereceitaitem) {
    Prereceitaitem.on('dataSourceAttached', function (obj) {
        Prereceitaitem.custom.autocomplete.atendimento = {
            where: function (reg) {
                return {
                    id: (reg),
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.id
                };
                return newRow;
            },
            label: function (row) {
                return row.id;
            }
        };

        Prereceitaitem.custom.autocomplete.medicamento = {
            where: function (reg) {
                return {
                  nome_apresentacao: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome_apresentacao
                };
                return newRow;
            },
            label: function (row) {
                return row.medicamento.nome_apresentacao;
            }
        };

        Prereceitaitem.custom.autocomplete.posologia = {
            where: function (reg) {
                return {
                    nome: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function (row) {
                return row.posologia.nome;
            }
        };

        Prereceitaitem.custom.autocomplete.dosagem = {
            where: function (reg) {
                return {
                    nome: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function (row) {
                return row.dosagem.nome;
            }
        };

        Prereceitaitem.custom.autocomplete.unidade_medida = {
            where: function (reg) {
                return {
                    nome: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function (row) {
                return row.unidade_medida.nome;
            }
        };

        Prereceitaitem.custom.autocomplete.via = {
            where: function (reg) {
                return {
                    nome: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function (row) {
                return row.via.nome;
            }
        };

        Prereceitaitem.custom.autocomplete.duracao = {
            where: function (reg) {
                return {
                    nome: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function (row) {
                return row.duracao.nome;
            }
        };

        Prereceitaitem.custom.autocomplete.frequencia = {
            where: function (reg) {
                return {
                    nome: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function (row) {
                return row.frequencia.nome;
            }
        };
    });
};
