'use strict';

module.exports = function(Prereceita) {

  Prereceita.customIncludes = ['PreReceitaItem'];

  Prereceita.on('dataSourceAttached', function(obj) {
    Prereceita.custom.autocomplete.medicamento = {
      where: function (reg) {
        return {
          nome: (reg)
        };
      },
      out: function (row) {
        var newRow = {
          id: row.id,
          label: row.nome
        };
        return newRow;
      },
      label: function (row) {
        return row.medicamento.nome;
      }
    };

    Prereceita.custom.autocomplete.posologia = {
      where: function (reg) {
        return {
          nome: (reg)
        };
      },
      out: function (row) {
        var newRow = {
          id: row.id,
          label: row.nome
        };
        return newRow;
      },
      label: function (row) {
        return row.posologia.nome;
      }
    };

    Prereceita.custom.autocomplete.dosagem = {
      where: function (reg) {
        return {
          nome: (reg)
        };
      },
      out: function (row) {
        var newRow = {
          id: row.id,
          label: row.nome
        };
        return newRow;
      },
      label: function (row) {
        return row.dosagem.nome;
      }
    };

    Prereceita.custom.autocomplete.unidade_medida = {
      where: function (reg) {
        return {
          nome: (reg)
        };
      },
      out: function (row) {
        var newRow = {
          id: row.id,
          label: row.nome
        };
        return newRow;
      },
      label: function (row) {
        return row.unidade_medida.nome;
      }
    };

    Prereceita.custom.autocomplete.via = {
      where: function (reg) {
        return {
          nome: (reg)
        };
      },
      out: function (row) {
        var newRow = {
          id: row.id,
          label: row.nome
        };
        return newRow;
      },
      label: function (row) {
        return row.via.nome;
      }
    };

    Prereceita.custom.autocomplete.duracao = {
      where: function (reg) {
        return {
          nome: (reg)
        };
      },
      out: function (row) {
        var newRow = {
          id: row.id,
          label: row.nome
        };
        return newRow;
      },
      label: function (row) {
        return row.duracao.nome;
      }
    };

    Prereceita.custom.autocomplete.frequencia = {
      where: function (reg) {
        return {
          nome: (reg)
        };
      },
      out: function (row) {
        var newRow = {
          id: row.id,
          label: row.nome
        };
        return newRow;
      },
      label: function (row) {
        return row.frequencia.nome;
      }
    };

    Prereceita.custom.autocompleteGeneral = {
      where: function (reg) {
        return {
          nome: (reg)
        };
      },
      out: function (row) {
        var newRow = {
          id: row.id,
          label: row.nome
        };
        return newRow;
      },
      label: function (row) {
        return row.nome;
      }
    };
  })

};
