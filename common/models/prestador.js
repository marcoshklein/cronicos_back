'use strict';

module.exports = function(Prestador) {
    Prestador.customIncludes = ['especialidades'];

    Prestador.validatesUniquenessOf('nome', {message: 'Prestador já existente!'});

    Prestador.on('dataSourceAttached', function(obj) {
        Prestador.custom.autocomplete.conselho = {
            where: function(reg) {
                return {
                    nome: (reg)
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function(row) {
                return row.conselho.nome;
            }
        };


        Prestador.custom.autocomplete.conselho_uf = {
            where: function(reg) {
                return {
                    nome: (reg)
                };
            },
            out: function(row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function(row) {
                return row.conselho_uf.nome;
            }
        };
    })

};
