'use strict';

var LoopBackContext = require('loopback-context');

module.exports = function (Paciente) {
    Paciente.customIncludes = ['programas', 'enderecos', 'cids'];
    // Paciente.setup(function(Paciente) {
    Paciente.on('dataSourceAttached', function (obj) {

        Paciente.custom.autocomplete.estado_civil = {
            where: function (reg) {
                return {
                    nome: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function (row) {
                return row.estado_civil.nome;
            }
        };
        Paciente.custom.autocomplete.nascimento_pais = {
            where: function (reg) {
                return {
                    nome: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function (row) {
                return row.nascimento_pais.nome;
            }
        };
        Paciente.custom.autocomplete.nascimento_uf = {
            where: function (reg) {
                return {
                    nome: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function (row) {
                return row.nascimento_uf.nome;
            }
        };
        Paciente.custom.autocomplete.nascimento_cidade = {
            where: function (reg) {
                return {
                    nome: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function (row) {
                return row.nascimento_cidade.nome;
            }
        };
        Paciente.custom.autocomplete.tipo_sangue = {
            where: function (reg) {
                return {
                    nome: (reg)
                };
            },
            out: function (row) {
                var newRow = {
                    id: row.id,
                    label: row.nome
                };
                return newRow;
            },
            label: function (row) {
                return row.tipo_sangue.nome;
            }
        };
    });

    Paciente.getProntuario = getProntuario;
    Paciente.remoteMethod('getProntuario', {
        description: 'Retrieves paciente\'s prontuario',
        accessType: 'READ',
        accepts: ([{
            arg: 'id',
            type: 'any',
            description: 'Model id',
            required: true,
            http: { source: 'path' }
        },]),
        returns: { arg: 'data', type: 'array', root: true },
        http: { verb: 'get', path: '/:id/prontuario' }
    });

    function getProntuario(id, fn) {
        console.log('get Prontuario!');

        var Atendimento = Paciente.app.models.Atendimento;
        Atendimento.find({
            include: [
                'prestador',
                'tipo',
                'status',
                {
                    'receituarios': {
                        'receituario_itens': ['medicamento', 'posologia', 'unidade_medida', 'via', 'frequencia', 'duracao']
                    }
                },
                {
                    'encaminhamentos': ['prestador', 'especialidade']
                }
            ],
            where: { pac_id: id },
            order: ['inicio DESC']
        }, function (err, rows) {
            if (err) {
                fn(err);
            }
            else {
                fn(null, rows);
            }
        });
    }

    Paciente.observe('before save', function pacienteBeforeSave(ctx, next) {

        if (ctx.instance && ctx.isNewInstance) { //update
            var instanceNome = ctx.instance.nome.toLowerCase();
            var instanceCPF = ctx.instance.cpf;
            var instanceData = ctx.instance.data_nascimento;

            var _ctx = LoopBackContext.getCurrentContext();
            var currentUser = _ctx && _ctx.get('currentUser');

            Paciente.find({
                    where: {
                        emp_id: currentUser.emp_id
                    }
                })
                .then(function (pacientes) {
                    pacientes.forEach(function (_paciente) {
                        var _pacienteNome = _paciente.nome.toLowerCase();
                        var _pacienteCPF = _paciente.cpf;
                        if (instanceNome === _pacienteNome && instanceCPF === _pacienteCPF && new Date(instanceData).getTime() === new Date(_paciente.data_nascimento).getTime()) {
                            var createErr = new Error("Esse paciente já existe! Insira outro com nome, CPF ou data de nascimento diferentes.");
                            createErr.statusCode = 422;
                            createErr.code = 'CANT_SAVE_MODEL';

                            next(createErr);
                            return;
                        }
                    });

                    next();
                });
        } else {
            next();
        }

    });

};
