// var caller = require('caller-id');
const defaultConfigs = require('../../utils/default/configs')();
const async = require('async');

module.exports = function(Config) {
  Config.validatesUniquenessOf('key');

  Config.cache = [];

  // it should return the cached config with that key
  Config.get = function(key) {
    if (Config.cache.length) {
      var conf = Config.cache.find(config => config.key === key).value;
      if (conf || conf === false){
        return conf;
      }
      else console.log('COULDNT FIND KEY: ', key);
    } else {
      console.log('config cache is empty');
      // console.log(caller.getData());
    }
  };

  // create database with default configs
  Config.createDefault = cb => {
    async.forEach(defaultConfigs, (cfg, callback) => {
      Config.create(cfg, err => {
        if (err) console.log(err);
        else callback();
      })
    }, err => {
      if (err) cb(err);
      else {
        Config.load(err => {
          if (err) cb(err);
          else cb();
        })
      }
    });
  };

  // it should load all the configs from the database and callback with error or success
  Config.load = cb => {
    Config.find({}, (err, rows) => {
      if (err) cb(err);
      else {
        Config.cache = rows;
        cb(null, rows);
      }
    })
  };

  // it should push new configs straight to the cache
  // Config.observe('after save', function(ctx, next) {
  // 	if (ctx.instance) {
  // 		Config.load(err => {
  // 			if(err) console.log('Error updating cache (after save)');
  // 			else console.log('Cache updated (after save)');
  // 		});
  // 	}
  // 	next();
  // });

  // --- REMOTE METHODS --

  // it should return configs with public:true
  Config.getPublicConfig = function(fn) {
    Config.find({
      where: {
        public: true
      }
    }, function(err, configs) {
      if (err) {
        console.log('Error getting public configs')
      } else {
        fn(null, configs)
      }
    })
  };

  Config.remoteMethod(
    'getPublicConfig', {
      description: 'Returns public config',
      accepts: [],
      returns: {
        arg: 'publicConfig',
        type: 'object',
        root: true,
        description: 'The response body contains the public config',
      },
      http: {
        verb: 'get',
        path: '/public'
      }
    }
  );

  //Refresh is only necessary when configs are deleted or modified in the database
  Config.refreshConfigs = function(fn) {
    Config.cache = [];
    Config.load(err => {
      if (!err) {
        console.log('Configs refreshed successfully');
        fn();
      } else {
        console.log('Error refreshing configs', err);
        fn(err);
      }
    })
  };

  Config.remoteMethod(
    'refreshConfigs', {
      description: 'Updates all configs accordingly to the configs in the database',
      accepts: [],
      returns: {},
      http: {
        verb: 'get',
        path: '/refresh'
      }
    }
  );

  Config.createMany = function(req, fn) {
    var cfgs = req;

    cfgs.forEach(cfg => {
      Config.create(cfg, err => {
        if (err) console.log(err)
      })
    });

    fn();
  };

  Config.remoteMethod(
    'createMany', {
      description: 'Creates many config objects at the same time',
      accepts: [{
        arg: 'req',
        type: 'object',
        required: true,
        http: {
          source: 'body'
        }
      }],
      returns: {},
      http: {
        verb: 'post',
        path: '/create-many'
      }
    }
  );

};
