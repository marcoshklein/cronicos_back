'use strict';

module.exports = function(Formularioespecialidade) {
  Formularioespecialidade.on('dataSourceAttached', function(obj) {
    Formularioespecialidade.custom.autocomplete.especialidade = {
      where: function(reg) {
        return {
          descricao: (reg),
          situacao: true
        };
      },
      out: function(row) {
        var newRow = {
          id: row.id,
          label: row.descricao
        };
        return newRow;
      },
      label: function(row) {
        return row.especialidade.descricao;
      }
    };
  })

};
