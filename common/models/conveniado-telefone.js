'use strict';

module.exports = function(Conveniadotelefone) {
  Conveniadotelefone.on('dataSourceAttached', function(obj) {
    Conveniadotelefone.custom.autocomplete.tipo_telefone = {
      where: function(reg) {
        return {
          nome: (reg)
        };
      },
      out: function(row) {
        var newRow = {
          id: row.id,
          label: row.nome
        };
        return newRow;
      },
      label: function(row) {
        return row.tipo_telefone.nome;
      }
    };
  });
};
