'use strict'

var app = require('../server/server');

exports.sendEmail = function sendEmail (email) {
    const Email = app.models.Email;

    console.log('Sending email to: ', email.to);

    return new Promise(function (resolve, reject) {
        Email.send(email, function(err, res) {
            if (err) {
                console.log('Error on sending email', err);
                reject(err);
            } else {
                console.log('Email sent successfully to: ', res.envelope.to);
                resolve(res);
            }
        });
    });

};
