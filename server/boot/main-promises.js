var foreignKeysBoot = require('./scripts/foreign-keys');
var userRolesBoot = require('./scripts/user-roles');

module.exports = function enableAuthentication(app) {
    userRolesBoot(app)
        .then(function success() {
            return foreignKeysBoot(app);
        })
        .then(function success() {
            var Config = app.models.Config;
            Config.load(function (err, configs) {
                if (err) console.log(err);
                else {
                    console.log('Configs loaded (CRON)');
                }
            });
        });
};
