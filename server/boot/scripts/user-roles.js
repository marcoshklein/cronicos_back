'use strict'

var path = require('path');

var populateModule = require(path.resolve(__dirname, '../../../bin/populate/populate.module'));

module.exports = function (app) {
    var Usuario = app.models.Usuario;
    var Role = app.models.Role;
    var RoleMapping = app.models.RoleMapping;

    var rolesList = new Array();
    var roleType = {};
    var roleIds = {
        admin: new Array(),
        prestador: new Array()
    };
    var promiseArrayPos = {
        admin: false,
        prestador: false
    };
    var _rmCount = 0;

    // Step 1. Get roles and check if they're existent
    return Role.find()
        .then(function (roles) {
            console.log('           ');
            console.log('------------------ Starting boot script for User Roles ------------------');
            console.log('           ');

            // If there isn't any role registered, run populate
            if (!roles.length) {
                console.log(_timestamp(), '(ROLEBOOTSCRIPT) Non-existent roles inside DB. Populating...');
                return populateModule.readPopulateRoles()
                  .then(function (addedRoles) {
                      rolesList = addedRoles;
                      return Usuario.find();
                  });
            }
            // Check role existence from rolesFromFile and fill list with roles not added to DB yet
            else {
                var _rolesToBeAdded = [];
                var _rolesFromFile = populateModule.getJSONroles();
                _rolesFromFile.forEach(function (_roleFile) {
                    var _existent = false;
                    roles.forEach(function (_roleDB) {
                        if (_roleDB.name === _roleFile.name) _existent = true;
                    });
                    if (!_existent) _rolesToBeAdded.push(_roleFile);
                });

                // Roles in database are in sync with JSON file. Continue...
                if (!_rolesToBeAdded.length) {
                    console.log(_timestamp(), '(ROLEBOOTSCRIPT) Roles in database are in sync with JSON file!');
                    rolesList = roles;
                    return Usuario.find();
                }
                // At least one role is non-existent. Add each to database before continue...
                else {
                    console.log(_timestamp(), '(ROLEBOOTSCRIPT) At least one non-existent roles inside DB. Populating specific ones...');
                    return populateModule.populateRoles(_rolesToBeAdded)
                      .then(function (addedRoles) {
                          rolesList = addedRoles;
                          return Usuario.find();
                      });
                }
            }
        })
        // Step 2. Get users and push promise array to check existence on Role.principals (RoleMapping) table
        // Checking only being done to admin and prestador role cases
        .then(function (users) {
            var roleMappingAdminPromises = [];
            var roleMappingPrestadorPromises = [];
            console.log(_timestamp(), '(ROLEBOOTSCRIPT) Checking admin and prestador role cases...');
            users.forEach(function (user) {
                if (user.admin) {
                    rolesList.forEach(function (role) {
                        if (role.name === 'admin') {
                            roleMappingAdminPromises.push(RoleMapping.findOne({where: { and: [ { roleId: role.id }, { principalId: user.id }] }}));
                            roleType['admin'] = role;
                            roleIds.admin.push(user.id);
                        }
                    });
                }

                if (user.pres_id) {
                    rolesList.forEach(function (role) {
                        if (role.name === 'prestador') {
                            roleMappingPrestadorPromises.push(RoleMapping.findOne({where: { and: [ { roleId: role.id }, { principalId: user.id }] }}));
                            roleType['prestador'] = role;
                            roleIds.prestador.push(user.id);
                        }
                    });
                }
            });

            if (roleMappingAdminPromises.length) {
                promiseArrayPos.admin = true;
            }
            if (roleMappingPrestadorPromises.length) {
                promiseArrayPos.prestador = true;
            }

            // If role mapping for admin and prestador are empty, return null to break promise chain cycle
            if (!roleMappingAdminPromises.length && !roleMappingPrestadorPromises.length) {
                console.log(_timestamp(), '(ROLEBOOTSCRIPT) There are no admin and prestador users!');
                return null;
            }
            else {
                var _rmadmin = Promise.all(roleMappingAdminPromises);
                var _rmprest = Promise.all(roleMappingPrestadorPromises);
                return Promise.all([_rmadmin, _rmprest]);
            }

        })
        // Step 3. If role maps are empty for that specific user that's an admin, add the role map to database table
        .then(function (roleMaps) {
            if (roleMaps !== null) {
                console.log(_timestamp(), '(ROLEBOOTSCRIPT) Checking admin and prestador role mappings for each user...');
                console.log(_timestamp(), '(ROLEBOOTSCRIPT) Existent RoleMaps: ', roleMaps.length);
                // console.log(roleMaps)
                var roleMapsPromises = new Array();
                roleMaps.forEach(function (typeMapArray, indexType) {
                    typeMapArray.forEach(function (roleMapping, indexUser) {
                        // console.log('innerTypeMapArray: ', innerTypeMapArray)
                        // Add case for admin
                        if (promiseArrayPos.admin && indexType === 0) {
                            // There isn't a role mapping for that position. So you need to add in roleMapping table
                            // for that specific ID from the specific type (eg. admin or prestador)
                            if (!roleMapping) {
                                var _promise = roleType['admin'].principals.create({
                                    principalType: RoleMapping.USER,
                                    principalId: roleIds['admin'][indexUser]
                                });
                                if (_promise) {
                                    roleMapsPromises.push(_promise);
                                    _rmCount++;
                                }
                            }
                            else if (roleMapping.principalId === roleIds.admin[indexUser] && roleMapping.roleId === roleType.admin.id) {
                                console.log(_timestamp(), '(ROLEBOOTSCRIPT) This user admin ID is registered in roleMappings: ', roleIds.admin[indexUser]);
                            }
                        }
                        // Add case for prestador
                        else if (promiseArrayPos.prestador && indexType === 1) {
                            // When there isn't any roleMapping, array'll be empty. So you need to add in roleMapping table
                            if (!roleMapping) {

                                var _promise = roleType['prestador'].principals.create({
                                      principalType: RoleMapping.USER,
                                      principalId: roleIds['prestador'][indexUser]
                                });
                                if (_promise) {
                                    roleMapsPromises.push(_promise);
                                    _rmCount++;
                                }
                            }
                            else if (roleMapping.principalId === roleIds.prestador[indexUser] && roleMapping.roleId === roleType.prestador.id) {
                                console.log(_timestamp(), '(ROLEBOOTSCRIPT) This user prestador ID is registered in roleMappings: ', roleIds.prestador[indexUser]);
                            }
                        }
                    });
                });

                if (roleMapsPromises.length) return roleMapsPromises;
                else {
                    console.log(_timestamp(), '(ROLEBOOTSCRIPT) No need to add any admin or prestador users in roleMapping table!');
                    return null;
                }
            }
            else return null;
        })
        .then(function (roleMaps) {
            if (roleMaps !== null) console.log(_timestamp(), '(ROLEBOOTSCRIPT) ' + _rmCount + ' RoleMapping necessary association roles created for each user role!');
            return true;
        })
        .catch(function (err) {
            console.log(_timestamp(), '(ROLEBOOTSCRIPT) ', err);
        })
        .finally(function () {
            console.log('           ');
            console.log('------------------ Ended boot script for User Roles ------------------');
            console.log('           ');
        });

};

function _timestamp() {
    var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
    return '[' + (new Date(Date.now() - tzoffset)).toISOString().slice(0, -1) + ']';
}
