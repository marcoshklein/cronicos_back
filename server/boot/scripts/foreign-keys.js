'use strict'

var fs = require('fs');
var path = require('path');

var _mixinsWithRelations = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../../data/relations-mixins.models.json'), 'utf8'));
var _dataSource = null;


module.exports = function (app) {
    var _checkModelPromises = new Array();
    var _auxRelationsSearched = new Array();
    var _fkCount = 0;

    if (!_dataSource) _dataSource = app.datasources.postgres;
    Object.keys(app.models).forEach(function (name) {
        if (!_isStopModel(name)) {
            var relations = _getRelations(app.models[name]);

            var mixins = _getMixins(app.models[name]);

            if (relations) {
                // Iterate relations, push search promise and push relation object to auxiliary array
                if (Object.keys(relations)) {
                    Object.keys(relations).forEach(function (_name) {
                        relations[_name].mainModel = name;
                        if (relations[_name].type === 'belongsTo' || relations[_name].type === 'hasOne') {
                            _checkModelPromises.push(_checkRelationFk(name, relations[_name].foreignKey));
                            _auxRelationsSearched.push(relations[_name]);
                        }
                        else if (relations[_name].type === 'hasMany') {
                            _checkModelPromises.push(_checkRelationFk(relations[_name].model, relations[_name].foreignKey));
                            _auxRelationsSearched.push(relations[_name]);
                        }
                    });
                }
            }

            if (mixins) {
                var relationsMixins = _checkRelationsFromMixins(name, mixins);
                if (relationsMixins) {
                    // Iterate mixins relations, push search promise and push each relation object to auxiliary array
                    if (Object.keys(relationsMixins)) {
                        Object.keys(relationsMixins).forEach(function (_name) {
                          relationsMixins[_name].mainModel = name;
                          if (relationsMixins[_name].type === 'belongsTo' || relationsMixins[_name].type === 'hasOne') {
                                _checkModelPromises.push(_checkRelationFk(name, relationsMixins[_name].foreignKey));
                                _auxRelationsSearched.push(relationsMixins[_name]);
                            }
                            else if (relationsMixins[_name].type === 'hasMany') {
                                _checkModelPromises.push(_checkRelationFk(relationsMixins[_name].model, relationsMixins[_name].foreignKey));
                                _auxRelationsSearched.push(relationsMixins[_name]);
                            }
                        });
                    }
                }
            }
        }

        // console.log('Relations for ' + name + ': ', _getRelations(app.models[name]));
        // console.log('Mixins for ' + name + ': ', _getMixins(app.models[name]));
    });

    return Promise.all(_checkModelPromises)
        .then(function (searchResults) {
            // console.log('           ');
            console.log('------------------ Starting boot script for Foreign Keys ------------------');
            var _addModelPromises = new Array();
            searchResults.forEach(function (count, index) {
                if (!count) {
                    var _promise = _addRelationFk(_auxRelationsSearched[index].mainModel, _auxRelationsSearched[index].model, _auxRelationsSearched[index].foreignKey);
                    if (_auxRelationsSearched[index].type === 'hasMany') _promise = _addRelationFk(_auxRelationsSearched[index].model, _auxRelationsSearched[index].mainModel, _auxRelationsSearched[index].foreignKey);
                    _addModelPromises.push(_promise);
                }
            });
            if (!_addModelPromises.length) return null;
            else return Promise.all(_addModelPromises);
        })
        .then(function (statusAdded) {
            if (statusAdded) {
                statusAdded.forEach(function (status) {
                    if (status) _fkCount++;
                });
            }

            if (!_fkCount) console.log(_timestamp(), '(FKBOOTSCRIPT) No foreign key constraints added!');
            else console.log(_timestamp(), '(FKBOOTSCRIPT) ' + _fkCount + ' necessary foreign keys created!');
        })
        .catch(function (err) {
            console.log(_timestamp(), '(FKBOOTSCRIPT)', err);
        })
        .finally(function () {
            console.log('           ');
            console.log('------------------ Ended boot script for Foreign Keys ------------------');
            console.log('           ');
        });

};

/**
 * Get relations from model definition
 * @param model
 * @returns {Object}
 * @private
 */
function _getRelations(model) {
    return model.definition.settings.relations;
}

/**
 * Get mixins from model definition
 * @param model
 * @returns {Object}
 * @private
 */
function _getMixins(model) {
    return model.definition.settings.mixins;
}

/**
 * Check relations from mixins (eg. Empresa in CompanyScope)
 * @param name
 * @param mixins
 * @returns {Object}
 * @private
 */
function _checkRelationsFromMixins(name, mixins) {
    var _relationsMixins = {};

    Object.keys(mixins).forEach(function (_originalMixinName) {
        Object.keys(_mixinsWithRelations).forEach(function (_relationMixinName) {
            if (_originalMixinName === _relationMixinName) {
                _relationsMixins[_relationMixinName] = _mixinsWithRelations[_relationMixinName];
            }
        });
    });

    return _relationsMixins;
}

/**
 * Check which models shouldn't be added foreign keys
 * @param name
 * @returns {boolean}
 * @private
 */
function _isStopModel(name) {
    return (name === 'UserIdentity' || name === 'UserCredential' || name === 'Prontuario');
}

/**
 * Run SQL query to check foreign key relation
 * @param name
 * @param relationName
 * @returns {Promise}
 * @private
 */
function _checkRelationFk(name, foreignKey) {
    var _fk = name.toLowerCase() + "_" + foreignKey.toLowerCase() + "_fkey";
    var _sql = "SELECT count(1)" +
        " FROM information_schema.table_constraints" +
        " WHERE constraint_name='" + _fk + "' AND table_name='" + name.toLowerCase() + "';";

    return new Promise(function (resolve, reject) {
        _dataSource.connector.execute(_sql, null, function (err, result) {
            if (err) reject(err);
            else resolve(parseInt(result[0].count));
        });
    })
    .catch(function (err) {
        console.log(_timestamp(), '(FKBOOTSCRIPT) Error when checking relation foreign key:', err);
    });
}

/**
 * Run SQL query to add foreign key relation
 * @param mainTable
 * @param refTable
 * @param foreignKey
 * @returns {Promise}
 * @private
 */
function _addRelationFk(mainTable, refTable, foreignKey) {
    var _fk = mainTable.toLowerCase() + "_" + foreignKey.toLowerCase() + "_fkey";
    var _sql = "ALTER TABLE " + mainTable.toLowerCase() +
        " ADD CONSTRAINT " + _fk +
        " FOREIGN KEY ("+ foreignKey.toLowerCase() + ")" +
        " REFERENCES " + refTable.toLowerCase() + "(id);";

    return new Promise(function (resolve, reject) {
        _dataSource.connector.execute(_sql, null, function (err, result) {
            if (err) reject(err);
            else resolve(result);
        });
    })
    .catch(function (err) {
        if (err.code !== '40P01') {
            console.log('           ');
            console.log(_timestamp(), '(FKBOOTSCRIPT) Error when adding relation foreign key:', err.message, '| Details:', err.detail);
            console.log(_timestamp(), '(FKBOOTSCRIPT) SQL that caused the error:', _sql)
        }
    });
}

function _timestamp() {
    var tzoffset = (new Date()).getTimezoneOffset() * 60000; //offset in milliseconds
    return '[' + (new Date(Date.now() - tzoffset)).toISOString().slice(0, -1) + ']';
}
