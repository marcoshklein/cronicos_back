var path = require('path');

var app = require(path.resolve(__dirname, '../server/server'));
var ds = app.datasources.postgres;

// var migrateList = ['Medicamento','PacienteCid', 'Atendimento', 'AtendimentoReceituarioItem', 'AtendimentoFormularioResposta'];
var migrateList = ['Menu'];
// var migrateList = ['AtendimentoFormularioResposta'];


migrateList.forEach(function(model) {
    ds.automigrate(model, function(err) {
        if (err) throw err;
    });
});

// ds.automigrate('TipoAtendimento', function(err) {
//     if (err) throw err;

//     var rows = [{
//             id: 1,
//             nome: 'Consulta'
//         },
//         {
//             id: 2,
//             nome: 'Retorno'
//         }
//     ];
//     var count = rows.length;
//     rows.forEach(function(_row) {
//         app.models.TipoAtendimento.create(_row, function(err, model) {
//             if (err) throw err;

//             console.log('Created:', model);

//             count--;
//             if (count === 0)
//                 ds.disconnect();
//         });
//     });
// });