var path = require('path');
var app = require(path.resolve(__dirname, '../../server/server'));
var classeTerapeutica = require(path.resolve(__dirname, '../data_medicamentos/classe-terapeutica-medicamento.json'));

classeTerapeutica.forEach(function(_row) {
  app.models.ClasseTerapeuticaMedicamento.create(_row, function(err, model) {
    if (err) throw err;
    console.log('Created: ', model);
  });
});
