'use strict'

var fs = require('fs');
var path = require('path');

var app = require(path.resolve(__dirname, '../../server/server'));
var rolesFromFile = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../data/role.json'), 'utf8'));

function populateRoles(_roles) {
    var _rolesPromises = new Array();

    return new Promise(function (resolve, reject) {
        _roles.forEach(function (_role) {
            _rolesPromises.push(app.models.Role.create(_role));
        });
        Promise.all(_rolesPromises)
          .then(function success(_roles) {
              var _addedRoles = new Array();
              _roles.forEach(function (_role) {
                  console.log('Created role: ', _role);
                  _addedRoles.push(_role);
              });
              resolve(_addedRoles);
          })
          .catch(function error(err) {
              reject(err);
          });
    });
}

function readPopulateRoles() {
    return populateRoles(rolesFromFile);
}

function getJSONroles() {
    return rolesFromFile;
}

module.exports = {
    populateRoles: populateRoles,
    readPopulateRoles: readPopulateRoles,
    getJSONroles: getJSONroles
};
