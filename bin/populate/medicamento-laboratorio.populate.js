var path = require('path');
var app = require(path.resolve(__dirname, '../../server/server'));
var laboratoriosMedicamento = require(path.resolve(__dirname, '../data_medicamentos/laboratorio-medicamento.json'));

laboratoriosMedicamento.forEach(function(_row) {
  app.models.LaboratorioMedicamento.create(_row, function(err, model) {
      if (err) throw err;
      console.log('Created: ', model);
    });
});
