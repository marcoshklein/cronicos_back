var path = require('path');
var app = require(path.resolve(__dirname, '../../server/server'));
var atendimentoStatus = require(path.resolve(__dirname, '../data/status-atendimento.json'));

var count = atendimentoStatus.length;

atendimentoStatus.forEach(function (_row) {
  app.models.StatusAtendimento.create(__row, function (err, model) {
    if (err) throw err;

    console.log('Created: ', model);

    count--;
  });
});
