var path = require('path');
var app = require(path.resolve(__dirname, '../../server/server'));
var medicamentos = require(path.resolve(__dirname, '../data_medicamentos/medicamento.json'));
var Promise = require('bluebird');
var request = Promise.promisifyAll(require('request'));

function getLaboratorios() {
  return new Promise((resolve) => {
    app.models.LaboratorioMedicamento.find().then(function (result) {
      resolve(result);
    });
  });
}

function getTipos() {
  return new Promise((resolve) => {
    app.models.TipoMedicamento.find().then(function (result) {
      resolve(result);
    });
  });
}

function getClasseTerapeutica() {
  return new Promise((resolve) => {
    app.models.ClasseTerapeuticaMedicamento.find().then(function (result) {
      resolve(result);
    });
  });
}

function getPrincipioAtivo() {
  return new Promise((resolve) => {
    app.models.PrincipioAtivoMedicamento.find().then(function (result) {
      resolve(result);
    });
  });
}

function getProduto() {
  return new Promise((resolve) => {
    app.models.ProdutoMedicamento.find().then(function (result) {
      resolve(result);
    });
  });
}

function getTarja() {
  return new Promise((resolve) => {
    app.models.TarjaMedicamento.find().then(function (result) {
      resolve(result);
    });
  });
}

function populateClasseterapeutica() {
  return new Promise((resolve) => {
    var classeTerapeutica = require(path.resolve(__dirname, '../data_medicamentos/classe-terapeutica-medicamento.json'));
    var count = classeTerapeutica.length;
    classeTerapeutica.forEach(function (_row) {
      app.models.ClasseTerapeuticaMedicamento.create(_row, function (err, model) {
        if (err) throw err;
        //console.log('Created: ', model);
        count--;
        console.log(count);
        if (count == 0)
          resolve('finish classe');
      });
    });
  });
}

function populateProduto() {
  return new Promise((resolve) => {
    var Produto = require(path.resolve(__dirname, '../data_medicamentos/produto-medicamento.json'));
    var count = Produto.length;
    Produto.forEach(function (_row) {
      app.models.ProdutoMedicamento.create(_row, function (err, model) {
        if (err) throw err;
        //console.log('Created: ', model);
        count--;
        console.log(count);
        if (count == 0)
          resolve('finish produto');
      });
    });
  });
}

function populateLaboratorio() {
  return new Promise((resolve) => {
    var Laboratorio = require(path.resolve(__dirname, '../data_medicamentos/laboratorio-medicamento.json'));
    var count = Laboratorio.length;
    Laboratorio.forEach(function (_row) {
      app.models.LaboratorioMedicamento.create(_row, function (err, model) {
        if (err) throw err;
        //console.log('Created: ', model);
        count--;
        console.log(count);
        if (count == 0)
          resolve('finish laboratorio');
      });
    });
  });
}

function populateTarja() {
  return new Promise((resolve) => {
    var Tarja = require(path.resolve(__dirname, '../data_medicamentos/tarja-medicamento.json'));
    var count = Tarja.length;
    Tarja.forEach(function (_row) {
      app.models.TarjaMedicamento.create(_row, function (err, model) {
        if (err) throw err;
        //console.log('Created: ', model);
        count--;
        console.log(count);
        if (count == 0)
          resolve('finish tarja');
      });
    });
  });
}

function populateTipo() {
  return new Promise((resolve) => {
    var Tipo = require(path.resolve(__dirname, '../data_medicamentos/tipo-medicamento.json'));
    var count = Tipo.length;
    Tipo.forEach(function (_row) {
      app.models.TipoMedicamento.create(_row, function (err, model) {
        if (err) throw err;
        //console.log('Created: ', model);
        count--;
        console.log(count);
        if (count == 0)
          resolve('finish tipo');
      });
    });
  });
}

function populatePrincipioAtivo() {
  return new Promise((resolve) => {
    var PrincipioAtivo = require(path.resolve(__dirname, '../data_medicamentos/principio-ativo-medicamento.json'));
    var count = PrincipioAtivo.length;
    PrincipioAtivo.forEach(function (_row) {
      app.models.PrincipioAtivoMedicamento.create(_row, function (err, model) {
        if (err) throw err;
        //console.log('Created: ', model);
        count--;
        console.log(count)
        if (count == 0)
          resolve('finish principio');
      });
    });

  });
}



function populate() {
  var objIndex = { 0: 'id_laboratorio', 1: 'id_tipo', 2: 'id_classe_terapeutica', 3: 'id_principio_ativo', 4: 'id_produto', 5: 'id_tarja', }
  var promises = [];
  var promises_populate = [];
  var objAux = { 0: {}, 1: {}, 2: {}, 3: {}, 4: {}, 5: {} }
  promises_populate.push(populatePrincipioAtivo());
  promises_populate.push(populateProduto());
  promises_populate.push(populateTarja());
  promises_populate.push(populateTipo());
  promises_populate.push(populateLaboratorio());
  promises_populate.push(populateClasseterapeutica());
  promises.push(getLaboratorios());
  promises.push(getTipos());
  promises.push(getClasseTerapeutica());
  promises.push(getPrincipioAtivo());
  promises.push(getProduto());
  promises.push(getTarja());

  Promise.all(promises_populate)
    .then(function (results) {
      console.log('Tabelas Secundárias inseridas');
      console.log(results);

      Promise.all(promises)
        .then(function (results) {
          //console.log("All done", results);
          console.log('Populando objAux para realizar as edições em medicamentos.json')
          results.forEach(function (tables, indexResult) {
            //tables = resultado do find de uma tabela
            //indexResult = index para saber qual tabela é
            tables.forEach(function (row) {
              // row = elemento da tabela pega no banco
              objAux[indexResult][row.nome] = row.id;
            });
          });
          // apos popular objAux
          console.log('Editando em medicamentos.json');
          Object.keys(objIndex).forEach(function (index) {
            // objIndex[index] = id na tabela
            console.log('Editando todos os', objIndex[index]);
            medicamentos.forEach(function (medicamento, indexMedicamento) {
              // medicamentos = json de medicamentos para populate
              // medicamento = obj de medicamento
              // indexMedicamento = index dele
              // medicamentos[Object...= Valor de cada medicamento já no json medicamentos (para editar direto nele)
              medicamentos[Object.keys(medicamentos)[indexMedicamento]][objIndex[index]] = objAux[index][medicamento[objIndex[index]]];
            });
          });
          console.log('Inserindo valores no banco');
          medicamentos.forEach(function (_row) {
            app.models.Medicamento.create(_row, function (err, model) {
              if (err) throw err;
              console.log('Created: ', model);
            });
          });
        })
        .catch(function (e) {
          console.log(e);
        });

    }).catch(function (e) {
      console.log(e)
    });

}

populate();
