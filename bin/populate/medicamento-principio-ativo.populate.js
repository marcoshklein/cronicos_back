var path = require('path');
var app = require(path.resolve(__dirname, '../../server/server'));
var principiosAtivo = require(path.resolve(__dirname, '../data_medicamentos/principio-ativo-medicamento.json'));

principiosAtivo.forEach(function (_row) {
  app.models.PrincipioAtivoMedicamento.create(_row, function (err, model) {
    if (err) throw err;
    console.log('Created: ', model);
  });
});
