var path = require('path');

var app = require(path.resolve(__dirname, '../server/server'));
var ds = app.datasources.postgres;

var migrateList = ['Indicador', 'IndicadorTabelaResultado', 'Formulario', 'FormularioIndicador'];

migrateList.forEach(function(model) {
    ds.automigrate(model, function(err) {
        if (err) throw err;
    });
});

ds.automigrate('IndicadorTipo', function(err) {
    if (err) throw err;

    var rows = [{
            id: 1,
            nome: 'Numérico'
        },
        {
            id: 2,
            nome: 'Texto'
        },
        {
            id: 3,
            nome: 'Resultado'
        }, {
            id: 4,
            nome: 'Alternativa S/N'
        }, {
            id: 5,
            nome: 'Lista'
        }, {
            id: 6,
            nome: 'Multiplas Alternativas'
        }
    ];
    var count = rows.length;
    rows.forEach(function(_row) {
        app.models.IndicadorTipo.create(_row, function(err, model) {
            if (err) throw err;

            console.log('Created:', model);

            count--;
            if (count === 0)
                ds.disconnect();
        });
    });
});